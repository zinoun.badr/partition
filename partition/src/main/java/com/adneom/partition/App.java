package com.adneom.partition;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.adneom.partition.exception.TechnicalException;


public class App 
{
   
    
    public  static List<List<String>> partition( final List<String> list, final int chunkSize) throws TechnicalException {
  
            if(Objects.isNull(list) || list.isEmpty())  {
            	throw new TechnicalException("Partionned list can not be null or empty");
            }
            
            if(list.size() < chunkSize)  {
            	throw new TechnicalException("chunkSize can not be > than partition size");
            }
    	
    	return  list.stream()
                        .collect(Collectors.groupingBy(s -> list.indexOf(s) / chunkSize))
                        .values().stream().collect(Collectors.toList());

    }
}
