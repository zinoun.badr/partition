package com.adneom.partition;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Test;

import com.adneom.partition.exception.TechnicalException;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

	/**
	 * Test 01 : partition([1,2,3,4,5], 2) return: [ [1,2],[3,4], [5] ]
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testPartitionArray_nominalTest01() throws TechnicalException {

		List<String> input = Arrays.asList("1", "2", "3", "4", "5");

		List<List<String>> output = App.partition(input, 2);

		assertTrue(!Objects.isNull(output));

		assertEquals(output.size(), 3);

		assertEquals(output.get(0).size(), 2);
		assertEquals(output.get(0), Arrays.asList("1", "2"));

		assertEquals(output.get(1).size(), 2);
		assertEquals(output.get(1), Arrays.asList("3", "4"));

		assertEquals(output.get(2).size(), 1);
		assertEquals(output.get(2), Arrays.asList("5"));

	}

	/**
	 * Test 02 : partition([1,2,3,4,5], 3) return: [ [1,2,3], [4,5] ]
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testPartitionArray_nominalTest02() throws TechnicalException {

		List<String> input = Arrays.asList("1", "2", "3", "4", "5");

		List<List<String>> output = App.partition(input, 3);

		assertTrue(!Objects.isNull(output));

		assertEquals(output.size(), 2);

		assertEquals(output.get(0).size(), 3);
		assertEquals(output.get(0), Arrays.asList("1", "2", "3"));

		assertEquals(output.get(1).size(), 2);
		assertEquals(output.get(1), Arrays.asList("4", "5"));

	}

	/**
	 * Test nominal 03 Description : partition([1,2,3,4,5], 1) return: [ [1], [2],
	 * [3], [4], [5] ]
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testPartitionArray_nominalTest03() throws TechnicalException {
		List<String> input = Arrays.asList("1", "2", "3", "4", "5");
		List<List<String>> output = App.partition(input, 1);

		assertTrue(!Objects.isNull(output));
		assertEquals(output.size(), 5);
		output.forEach(s -> assertEquals(s.size(), 1));

		assertEquals(output.get(0), Arrays.asList("1"));
		assertEquals(output.get(1), Arrays.asList("2"));
		assertEquals(output.get(2), Arrays.asList("3"));
		assertEquals(output.get(3), Arrays.asList("4"));
		assertEquals(output.get(4), Arrays.asList("5"));

	}

	/**
	 * Test technique 01 Description : partition null return: Technical Exception,
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testPartitionExeception01() throws TechnicalException {

		List<String> input = null;

		try {
			App.partition(input, 1);
			fail("Should return business Exception");
		} catch (TechnicalException e) {

			assertEquals(e.getMessage(), "Partionned list can not be null or empty");

		}

	}
	
	/**
	 * Test technique 02 Description : chunck size > partition list size  return: Technical Exception,
	 * 
	 * @throws TechnicalException
	 */
	@Test
	public void testPartitionExeception02() throws TechnicalException {

		List<String> input = Arrays.asList("1", "2", "3", "4", "5");

		try {
			App.partition(input, 10);
			fail("Should return business Exception");
		} catch (TechnicalException e) {

			assertEquals(e.getMessage(), "chunkSize can not be > than partition size");

		}

	}
}
