# Partition Library

[![pipeline status](https://gitlab.com/zinoun.badr/partition/badges/master/pipeline.svg)](https://gitlab.com/zinoun.badr/partition/commits/master)
[![coverage report](https://gitlab.com/zinoun.badr/partition/badges/master/coverage.svg)](https://gitlab.com/zinoun.badr/partition/commits/master)

## Requirements

For development, you will need:
- [JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3.2+](https://maven.apache.org/download.cgi)
- Preferable IDE:
    - [STS](https://spring.io/guides/gs/sts/)
    - [IntelliJ IDEA](https://www.jetbrains.com/idea/download)
    - [VSCode](https://code.visualstudio.com/) 

---

## Run app
-   `$ git clone https://gitlab.com/zinoun.badr/partition.git`
- mvn clean install